import json

with open("meal.json", "r") as file:
    meal_data = json.load(file)

count = 1
for key in meal_data:
    meal_id = count
    title = key['title']
    preptime = key['preptime']
    cooktime = key['cooktime']
    materials = key['materials']
    ingredients = key['ingredients']
    directions = key['directions']

    print("<head>")
    print("\t<title>%s</title>" % title)
var thing = document.getElementById('calendar-link');
thing.addEventListener("click", () => {
	var calendar = document.getElementById('calendar-month-1');
	var newDisplay = calendar.style.display == 'flex' ? 'none' : 'flex';
	calendar.style.display = newDisplay;
});
import json
from models import app, db, Meal, Recipe

def readMeal():
    with open("data.json", "r") as file:
            meal_data = json.load(file)

    meal_count = 1
    recipe_count = 1
    for meal in meal_data:
        meal_id = meal_count
        meal_count += 1
        domain_meal = Meal(meal_id)
        db.session.add(domain_meal)
        for key in meal['Details']:
            recipe_id = recipe_count
            recipe_count += 1
            title = key['title']
            preptime = key['preptime']
            cooktime = key['cooktime']
            materials = key['materials']
            ingredients = key['ingredients']
            directions = key['directions']
            if directions[-1] == '':
                directions = directions[:-1]

            recipe = Recipe(recipe_id, meal_id, title, preptime, cooktime, materials, ingredients, directions)
        
            db.session.add(recipe)
        db.session.commit()

readMeal()

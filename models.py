from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB_STRING",'postgres://postgres:abc123@localhost:5432/mydb')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
db = SQLAlchemy(app)

class Meal(db.Model):
    __tablename__ = "meals"
    id = db.Column(db.Integer, primary_key = True, nullable = False)

    def __init__(self, meal_id):
        self.id = meal_id

class Recipe(db.Model):
    __tablename__ = "recipes"
    id = db.Column(db.Integer, primary_key = True, nullable = False)
    meal_id = db.Column(db.Integer, nullable = False)
    title = db.Column(db.String)
    preptime = db.Column(db.String)
    cooktime = db.Column(db.String)
    materials = db.Column(db.String)
    ingredients = db.Column(db.PickleType)
    directions = db.Column(db.PickleType)

    def __init__(self, id, meal_id, title, preptime, cooktime, materials, ingredients, directions):
        self.id = id
        self.meal_id = meal_id
        self.title = title
        self.preptime = preptime
        self.cooktime = cooktime
        self.materials = materials
        self.ingredients = ingredients
        self.directions = directions

db.drop_all()
db.create_all()

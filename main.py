from flask import render_template
from readData import app, db, Meal, readMeal
from flask import request
from sqlalchemy import desc
import subprocess

@app.route('/', methods=['GET'])
def home():
    return render_template('home.html')

@app.route('/faq', methods=['GET'])
def faq():
    return render_template('faq.html')

@app.route('/techniques', methods=['GET'])
def techniques():
    return render_template('techniques.html')

@app.route('/meals', methods=['GET'])
def meals():
	meals = db.session.query(Meal).all()
	return render_template('meals.html', meals=meals)

@app.route('/meal/<int:meal_id>', methods=['GET'])
def specifc_meal(meal_id):
    specific_meal = db.session.query(Meal).get(meal_id)
    print('specific meal!!', specific_meal)
    print(type(specific_meal))
    return render_template('meal.html', meal=specific_meal)

if __name__ == "__main__":
    app.run()
import json
import os

'''
[
	{
		"Title": ,
		"Preptime": ,
		"Cooktime": ,
		"Materials": ,
		"Ingridients": ,
		"Directions": ,
	}

]

[
	{
		"Meal": 1, 
		"Details": [
						{
							"Title": ,
							"Preptime": ,
							"Cooktime": ,
							"Materials": ,
							"Ingridients": ,
							"Directions": ,
						},
						{
							"Title": ,
							"Preptime": ,
							"Cooktime": ,
							"Materials": ,
							"Ingridients": ,
							"Directions": ,
						}
						{
							"Title": ,
							"Preptime": ,
							"Cooktime": ,
							"Materials": ,
							"Ingridients": ,
							"Directions": ,
						}
					]
	}
]
'''
day = 1
data = []
for subdir, dirs, files in os.walk("C:/Users/daveb_000/Desktop/Daily Dinners"):
	if subdir == 'C:/Users/daveb_000/Desktop/Daily Dinners':
		continue
	meal = {}
	meal["Meal"] = day
	day += 1
	details = []
	if len(files) == 0:
		continue
	for file in files:
		if ".txt" in str(file):
			d = {}
			f = os.path.join(subdir, file)
			text = open(f, "r")
			print(text)
			text = text.read()
			parts = text.split(":::")
			for i in range(len(parts)):
				parts[i] = parts[i].split('\n')
				for thing in parts[i]:
					if thing == '':
						parts[i].remove(thing)
			if len(parts[0]) > 1 or len(parts[0][0]) > 100:
				parts.remove(parts[0])
			title = parts[0][0]
			d["title"] = title

			times = parts[1]
			for time in times:
				if "rep" in time:
					preptime = time
				elif "ook" in time:
					cooktime = time
			preptime = preptime.strip('\t\t\t\t\t\t\t\t\t')
			preptime = preptime.replace('min', "Minutes")
			preptime = preptime.replace('hour', "Hour")
			d["preptime"] = preptime

			cooktime = cooktime.strip('\t\t\t\t\t\t\t\t\t')
			cooktime = cooktime.replace('min', "Minutes")
			cooktime = cooktime.replace('hour', "Hour")

			d["cooktime"] = cooktime

			materials = parts[2][0].strip("Equipment Needed: ")
			d["materials"] = materials

			ingredients = parts[3]
			d["ingredients"] = ingredients

			directions = parts[4]
			d["directions"] = directions

			details.append(d)

	meal["Details"] = details
	data.append(meal)
with open('data.json', 'w') as f:
	json.dump(data, f, ensure_ascii=False)
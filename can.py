template = '''
<html>
<head>
	<title>{}</title>
	<meta charset='utf-8'>
	<link rel="stylesheet" title="basic style" type="text/css" href="./book_recipe.css" media="all" />
	<link rel="stylesheet" title="basic style" type="text/css" href="./day.css" media="all" />
	<link rel="stylesheet" title="basic style" type="text/css" href="./calendar.css" media="all" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
	<div class="content-wrapper">
		<div id='header' style="color:{};">
			<img src='{}' style="width:192px;height:192px;margin-bottom:10px;">
			<div id="header-words">
				<span id="day-text">{}</span>
				<span class="title">{}</span>
				
				<div class="time-container">
					<img class="clock icon" alt="clock" src="{}">
					<div class="time-text">
						<span class="time" id="preptime">{}</span>
						<span id="cooktime" class="time">{}</span>
					</div>
				</div>
				<div class="equipment-container">
					<img id="pan" alt="pan" class="pan icon" src="pan2.png">
					<span id="equipment-text">{}</span>
				</div>
			</div>
		</div>

		<div class="lower-content">
			<div class="ingredients-container" style="background-color:{};">
			<div class="ingredients-list">
 				{}
			</div>
			</div>

			<div class="directions-container">
				{}
			</div>
		</div>
	</div>
</body>
</html>
'''

ingredient_template = '''
					<div class="ingredient">
						<img class="starburst-ingrident" src="{}">
						<span style="color:{};" class="ingredient-text">{}</span>
					</div>
'''

import os

rootdir = "C:/Users/daveb_000/Desktop/Daily Dinners/David's Mass Export"

for subdir, dirs, files in os.walk(rootdir):
    for file in files:
        filepath = subdir + os.sep + file
        if filepath.endswith(".txt"):
            file = open(filepath, encoding="utf8")
            text = file.read().split(':::')
            weekday = text[1]

            day = weekday[9:].strip()

            if day == 'Sunday':
            	color = '#ff8c11'
            	clock = 'clockOrange.png'
            	color2 = '#74842c'
            	icon = 'starburstAvo.png'
            elif day == 'Monday':
            	color ='#d4bd19'
            	clock = 'clockGold.png'
            	color2 = '#0c94a0'
            	icon = 'starburstDarkAqua.png'
            elif day == 'Tuesday':
            	color = '#c4d10d'
            	clock = 'clockChartreuse.png'
            	color2 = '#623a29'
            	icon = 'starburstBrown.png'
            elif day == 'Wednesday':
            	color = '#74842c'
            	clock = 'clockAvoGreen.png'
            	color2 = '#d4bd19'
            	icon = 'starburstGold.png'
            elif day == 'Thursday':
            	color = '#29cccf'
            	clock = 'clockLightAqua.png'
            	color2 = '#623a29'
            	icon = 'starburstBrownLA.png'
            elif day == 'Friday':
            	color = '#0c94a0'
            	clock = 'clockDarkAqua.png'
            	color2 = '#c4d10d'
            	icon = 'starburstChar.png'
            elif day == 'Saturday':
            	color = '#623a29'
            	clock = 'clockBrown.png'
            	color2 = '#29cccf'
            	icon = 'starburstLA.png'

            img_key = weekday.replace(',', '') + '.jpg'

            if len(text) == 7:
            	title = text[2].strip()
            	times = text[3].strip().split('\n')
            	equipment = text[4].strip()
            	ingredients = text[5].strip().split('\n')
            	direrctions = text[6].strip().split('\n')

            elif len(text) == 8:
            	title = text[3].strip()
            	times = text[4].strip().split('\n')
            	equipment = text[5].strip()
            	ingredients = text[6].strip().split('\n')
            	direrctions = text[7].strip().split('\n')
            
            file.close()

            preptime = times[0]
            cooktime = times[1].strip()

            ingredient_string = ''
            for ingredient in ingredients:
            	if ingredient == '':
            		continue
            	ingredient_string += ingredient_template.format(icon, color2, ingredient)

            direrctions_string = ''
            for direrction in direrctions:
            	direrctions_string += "<p>{}</p>".format(direrction)

            output = template.format(title, color, img_key, weekday, title, clock, preptime, cooktime, equipment, color, ingredient_string, direrctions_string)
            
            save_as = weekday + title + '.html'
            save_as = save_as.replace('\n', '').replace(' ', '-').replace(',', '')

            out_file = open(save_as, 'w', encoding="utf-8")
            out_file.write(output)
            out_file.close()
            #break